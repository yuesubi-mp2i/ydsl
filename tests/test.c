#include <stdio.h>

#include "ydsl.h"


typedef StructList(, int) List;
typedef StructQueue(, int) Queue;


int main()
{
	List l = {0};

	List_pushBack(&l, 42);
	List_pushBack(&l, 19);
	List_pushBack(&l, 149);
	assert(List_get(&l, 0) == 42);
	assert(List_get(&l, 1) == 19);
	assert(List_get(&l, 2) == 149);

	List_popBack(&l);
	assert(List_get(&l, 0) == 42);
	assert(List_get(&l, 1) == 19);

	List_popBack(&l);
	List_popBack(&l);
	assert(List_isEmpty(&l));

	List_free(&l);

	Queue q = {0};

	// Queue_push(&q, 42);
	// Queue_push(&q, 19);
	// Queue_push(&q, 149);

	for (int i = 0; i < 10; i++) {
		Queue_push(&q, i);
		for (int j = 0; j < q._capacity; j++)
			printf("%d, ", q._data[j]);
		printf("\n");
	}

	{
		printf("\n");
		for (int i = 0; i < 100; i++) {
			for (int j = 0; j < q._capacity; j++)
				printf("%d, ", q._data[j]);
			printf(" >%d\n", Queue_peak(&q));
			Queue_push(&q, Queue_pop(&q));
		}
	}

	Queue_free(&q);

	return 0;
}