.PHONY: tests
tests:
	mkdir -p out
	gcc $(shell find ./tests -name "*.c") -o out/test -I ydsl
	./out/test

.PHONY: tests-debug
tests-debug:
	mkdir -p out
	gcc $(shell find ./tests -name "*.c") -g -o out/test -I ydsl
	gdb ./out/test