#ifndef _YDSL_STACK_H
#define _YDSL_STACK_H

#include <assert.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdlib.h>

#include "defs.h"

#include "list.h"


#define StructStack(name, T) StructList(name, T)
#define Stack_isEmpty(s) List_isEmpty(s)
#define Stack_push(s, x) List_pushBack(s, x)
#define Stack_pop(s) List_popBack(s)
#define Stack_peak(s) List_get(s, (s)->len - 1)
#define Stack_peakPtr(s) List_getPtr(s, (s)->len - 1)
#define Stack_free(s) List_free(s)


#endif  // _YDSL_STACK_H