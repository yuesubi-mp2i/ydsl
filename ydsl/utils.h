#ifndef _YDSL_UTILS_H
#define _YDSL_UTILS_H

#include <stdbool.h>


#define _YDSL_MAX(a, b) (((a) > (b))? (a) : (b))
#define _YDSL_EXP2(exponent) (0b1 << (exponent))

#define _YDSL_HEAP_isRoot(index) ((index) == 0)
#define _YDSL_HEAP_leftOf(index) (2*((index) + 1) - 1)
#define _YDSL_HEAP_rightOf(index) (2*((index) + 1))

#define _YDSL_GetBit(byte, n) (((byte) >> (n)) & 0b1)

#define _YDSL_BitMap_getBit(bm, n) \
_YDSL_GetBit((bm)[(n) / sizeof(bool)], (n) % sizeof(bool))

#define _YDSL_BitMap_SetBit(bm, n, val)					\
do {									\
	(bm)[(n) / sizeof(bool)] &= ~(0b1 << ((n) % sizeof(bool)));	\
	(bm)[(n) / sizeof(bool)] |= ((val) << ((n) % sizeof(bool)));	\
} while (false)


#endif  // _YDSL_UTILS_H