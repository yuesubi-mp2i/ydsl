#ifndef _YDSL_QUEUE_H
#define _YDSL_QUEUE_H

#include <assert.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>

#include "defs.h"


#define StructQueue(name, T) 	\
struct name { 			\
	size_t len; 		\
	size_t _start; 		\
	size_t _capacity; 	\
	T *_data;		\
}


#define _YDSL_Queue_T(q) typeof(*(q)->_data)


#define Queue_isEmpty(q) ((q)->len == 0)


// Useful because memcpy on memory that overlaps is UB
#define _YDSL_LeftArrayCpy(dest, src, n)	\
do {						\
	for (size_t i = 0; i < (n); i++){	\
		(dest)[i] = (src)[i];		\
	}					\
} while (false)


#define Queue_push(q, x)						\
do {									\
	_YDSL_Queue_T(q) evaluated_x = (x);				\
									\
	if ((q)->_start + (q)->len >= (q)->_capacity) {			\
		size_t grow_capacity = ((q)->len + 1) * YDSL_GROW_FACTOR; \
									\
		if (grow_capacity > (q)->_capacity) {			\
			(q)->_capacity = grow_capacity;			\
									\
			(q)->_data = realloc(				\
				(q)->_data,				\
				sizeof(_YDSL_Queue_T(q)) * (q)->_capacity \
			);						\
			assert((q)->_data);				\
		}							\
									\
		_YDSL_LeftArrayCpy(					\
			(q)->_data,					\
			&((q)->_data[(q)->_start]),			\
			(q)->len					\
		);							\
		(q)->_start = 0;					\
	}								\
									\
	(q)->len++;							\
	(q)->_data[(q)->_start + (q)->len - 1] = evaluated_x;		\
} while (false)


#define Queue_pop(q)							\
({									\
	assert(!Queue_isEmpty(q) && "Queue_pop on empty queue!");	\
									\
	_YDSL_Queue_T(q) pop_val = (q)->_data[(q)->_start];		\
	(q)->len--;							\
	(q)->_start++;							\
									\
	size_t shrink_capacity = (q)->_capacity / YDSL_SHRINK_TRIGGER_FACTOR; \
									\
	if ((q)->len < shrink_capacity) { 				\
		(q)->_capacity = shrink_capacity;			\
		_YDSL_Queue_T(q) *new_data = malloc(sizeof(_YDSL_Queue_T(q)) * (q)->_capacity); \
		assert(new_data);					\
									\
		_YDSL_LeftArrayCpy(					\
			new_data,					\
			&((q)->_data[(q)->_start]),			\
			(q)->len					\
		);							\
		(q)->_start = 0;					\
	}								\
									\
	pop_val;							\
})


#define Queue_peak(q)							\
({									\
	assert(!Queue_isEmpty(q) && "Queue_peak on empty queue!");	\
	(q)->_data[(q)->_start];					\
})


#define Queue_peakPtr(q)						\
({									\
	assert(!Queue_isEmpty(q) && "Queue_peakPtr on empty queue!");	\
	&(q)->_data[(q)->_start];					\
})


#define Queue_free(q)		\
do {				\
	(q)->len = 0;		\
	(q)->_start = 0;	\
	(q)->_capacity = 0;	\
	free((q)->_data);	\
} while (false)			\


#endif  // _YDSL_QUEUE_H