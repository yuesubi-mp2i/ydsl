#ifndef _YDSL_LIST_H
#define _YDSL_LIST_H

#include <assert.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdlib.h>

#include "defs.h"
#include "utils.h"


#define StructList(name, T) 	\
struct name { 			\
	size_t len; 		\
	size_t _capacity; 	\
	T *_data;		\
}


#define _YDSL_List_T(l) typeof(*(l)->_data)


#define List_reserve(l, n)					\
do {								\
	(l)->_capacity = _YDSL_MAX((l)->_capacity, n);		\
	(l)->_data = realloc(					\
		(l)->_data,					\
		sizeof(_YDSL_List_T(l)) * (l)->_capacity	\
	);							\
	assert((l)->_data);					\
} while (false);


#define List_isEmpty(l) ((l)->len == 0)


#define List_pushBack(l, x)						\
do {									\
	_YDSL_List_T(l) evaluated_x = x;				\
	(l)->len++;							\
									\
	if ((l)->len >= (l)->_capacity) {				\
		(l)->_capacity = YDSL_GROW_FACTOR * (l)->len;		\
		(l)->_data = realloc(					\
			(l)->_data,					\
			sizeof(_YDSL_List_T(l)) * (l)->_capacity	\
		);							\
		assert((l)->_data);					\
	}								\
									\
	(l)->_data[(l)->len - 1] = evaluated_x;				\
} while (false)


#define List_popBack(l)							\
({									\
	assert(!List_isEmpty(l) && "List_popBack on empty list!");	\
									\
	(l)->len--;							\
	_YDSL_List_T(l) pop_val = (l)->_data[(l)->len];			\
									\
	size_t shrink_capacity = (l)->_capacity / YDSL_SHRINK_TRIGGER_FACTOR; \
									\
	if ((l)->len < shrink_capacity) {			 	\
		(l)->_capacity = shrink_capacity;			\
									\
		(l)->_data = realloc(					\
			(l)->_data,					\
			sizeof(_YDSL_List_T(l)) * (l)->_capacity	\
		);							\
		assert((l)->_data);					\
	}								\
									\
	pop_val;							\
})


#define List_get(l, i)							\
({									\
	assert(0 <= i && i < (l)->len && "List_get i out of bounds!");	\
	(l)->_data[i];							\
})


#define List_getPtr(l, i)						\
({									\
	assert(0 <= i && i < (l)->len && "List_getPtr i out of bounds!"); \
	&(l)->_data[i];							\
})


#define List_set(l, i, x)						\
do {									\
	_YDSL_List_T(l) evaluated_x = x;				\
	assert(0 <= i && i < (l)->len && "List_set i out of bounds!");	\
	(l)->_data[i] = evaluated_x;					\
} while (false)


#define List_setPtr(l, i, ptr)						\
do {									\
	_YDSL_List_T(l) *evaluated_ptr = ptr;				\
	assert(0 <= i && i < (l)->len && "List_setPtr i out of bounds!"); \
	(l)->_data[i] = *evaluated_ptr;					\
} while (false)


#define List_free(l)		\
do {				\
	(l)->len = 0;		\
	(l)->_capacity = 0;	\
	free((l)->_data);	\
} while (false)			\


#endif  // _YDSL_LIST_H