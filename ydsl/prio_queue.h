#ifndef _YDSL_PRIO_QUEUE_H
#define _YDSL_PRIO_QUEUE_H

#include <assert.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdlib.h>

#include "defs.h"
#include "utils.h"


#define StructPrioQueue(name, T) 					\
struct name { 								\
	size_t size; 							\
	size_t _height; 						\
	bool (*_is_ordered)(T a, T b); 					\
	/* Bitmap that contains the colors of the red-black tree */	\
	bool *_colors;							\
	T *_data;							\
}


#define _YDSL_PQ_T(q) typeof(*(q)->_data)

#define _YDSL_PQ_dataSize(q) 			\
	(((q)->size == 0)?			\
		0 : _YDSL_EXP2((t)->_height))

#define _YDSL_PQ_colorsSize(q)				\
({							\
	size_t data_size _YDSL_PQ_dataSize(q);		\
	size_t colors_size = data_size / sizeof(bool);	\
	if (data_size % sizeof(bool) != 0)		\
		colors_size++;				\
	colors_size;					\
})

#define _YDSL_PQ_exists(q, index) ((index) < (q)->size)

#define _YDSL_PQ_get(q, index) ((q)->_data[(index)])
#define _YDSL_PQ_set(q, index, x) (q)->_data[(index)] = (x)

#define _YDSL_PQ_getCol(q, index) _YDSL_BitMap_getBit((q)->_colors, index)

#define _YDSL_PQ_setCol(q, index, col) \
_YDSL_BitMap_SetBit((q)->_colors, index, col)


#define _YDSL_PQ_takeDown(q, index)					\
{									\
	bool is_down = false;						\
	size_t actual = index;						\
									\
	while (!is_down) {						\
		size_t left = _YDSL_HEAP_leftOf(actual);		\
		size_t right = _YDSL_HEAP_rightOf(actual);		\
									\
		_YDSL_PQ_T(q) max = actual;				\
									\
		if (_YDSL_PQ_exists(q, left) &&				\
			!(q)->_is_ordered(_YDSL_PQ_get(q, max),		\
				_YDSL_PQ_get(q, left)))			\
		{							\
			max = left;					\
		}							\
									\
		if (_YDSL_PQ_exists(q, right) &&			\
			!(q)->_is_ordered(_YDSL_PQ_get(q, max),		\
				_YDSL_PQ_get(q, right)))		\
		{							\
			max = right;					\
		}							\
									\
		if (max == actual) {					\
			is_down = true;					\
		} else {						\
			_YDSL_PQ_T(q) tmp = _YDSL_PQ_get(q, actual);	\
			_YDSL_PQ_set(q, actual, _YDSL_PQ_get(q, max));	\
			_YDSL_PQ_set(q, max, tmp);			\
									\
			actual = max;					\
		}							\
	}								\
}


#define PrioQueue_new(is_ordered_func)	\
{					\
	0, 0,				\
	(is_ordered_func)		\
	NULL, NULL			\
}


#define PrioQueue_push(q, x)
do {
	_YDSL_PQ_T(q) to_place = x;
	(q)->size++;

	if (size > _YDSL_PQ_dataSize(q)) {
		(q)->_data = realloc(sizeof(_YDSL_PQ_T) * _YDSL_PQ_dataSize(q));
		(q)->_height++;
	}

	size_t index = 0;
	size_t depth = 0;

	while (depth < (q)->height) {
		size_t left = _YDSL_HEAP_leftOf(index);

		if (_YDSL_PQ_getCol(q, left) == YDSL_RED){
			_YDSL_PQ_setCol(q, left, YDSL_BLACK);
			// go down right
		} else {
			// go down left
		}
	}

} while (false)


#define List_reserve(l, n)

#endif  // _YDSL_PRIO_QUEUE_H